﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace UGit
{
    public class GitRepository
    {
        public string Name { get; set; }

        public string Path { get; set; }

        public string RemoteName { get; set; } = "origin";

        public string RemoteUri { get; set; }

        public string Branch { get; set; } = "master";


        [JsonIgnore]
        public static Credentials Credentials { get; set; }

        public GitRepository(string name, string path, string remoteUri)
        {
            Name = name;
            Path = path;
            RemoteUri = remoteUri;
        }

        #region Methods

        public void UpdateRepository()
        {
            // Set git credentials
            ExecuteGitCommand("config user.name \"" + Credentials.Name + "\"");
            ExecuteGitCommand("config user.email \"" + Credentials.Email + "\"");

            // Set configured remote
            string result = ExecuteGitCommand("remote set-url " + RemoteName + " " + RemoteUri);
            if (result.Contains("fatal"))
            {
                result = ExecuteGitCommand("remote add " + RemoteName + " " + RemoteUri);
            }

            // Fetch from all remotes, if there is an internet connection
            if (Helper.CheckInternetConnection())
            {
                ExecuteGitCommand($"fetch --all");
            }
        }

        public bool Clone()
        {
            return ExecuteGitCommand("clone " + "\"" + RemoteUri + "\"" + " " + "\"" + Path + "\"").Contains("fatal:");
        }

        public string GetLog()
        {
            return ExecuteGitCommand("log --stat -10");
        }

        public RepositoryStatus GetStatus()
        {
            var statusString = ExecuteGitCommand("status");

            return new RepositoryStatus()
            {
                IsConnected = Helper.CheckInternetConnection(),
                IsCloned = (Directory.Exists(Path) && !statusString.Contains("fatal: Not a git repository")),
                IsAhead = statusString.Contains("ahead"),
                IsBehind = statusString.Contains("behind"),
                IsDiverged = statusString.Contains("diverged"),
                IsChanged = !statusString.Contains("nothing to commit, working tree clean"),
                Raw = statusString
            };
        }

        public void OpenGitConsole()
        {
            Process gitConsoleProcess = new Process();
            gitConsoleProcess.StartInfo = new ProcessStartInfo()
            {
                FileName = "powershell.exe",
                WorkingDirectory = Path,
                RedirectStandardOutput = false,
                RedirectStandardError = false,
            };
            gitConsoleProcess.Start();
        }

        private string ExecuteGitCommand(string arguments)
        {
            if (!Directory.Exists(Path) && !arguments.StartsWith("clone"))
            {
                return "Error at " + Name + ": Directory not found";
            }

            Process gitProcess = new Process();
            gitProcess.StartInfo = new ProcessStartInfo()
            {
                FileName = "git",
                Arguments = arguments,
                WorkingDirectory = arguments.StartsWith("clone") ? "" : Path,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                CreateNoWindow = true
            };
            gitProcess.Start();

            return gitProcess.StandardOutput.ReadToEnd() + gitProcess.StandardError.ReadToEnd();
        }

        #endregion Methods
    }

    public class Credentials
    {
        public string Email { get; set; }

        public string Name { get; set; }

        public Credentials(string name, string email)
        {
            Name = name;
            Email = email;
        }
    }

    public class RepositoryStatus
    {
        public bool IsConnected { get; set; }
        public bool IsCloned { get; set; }
        public bool IsAhead { get; set; }
        public bool IsBehind { get; set; }
        public bool IsDiverged { get; set; }
        public bool IsChanged { get; set; }

        public string Raw { get; set; }
    }
}