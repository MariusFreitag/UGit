﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;

namespace UGit
{
    public class GitLab
    {
        private const string ApiUrl = "https://gitlab.com/api/v3";

        private readonly string _privateToken;

        public GitLab(string privateToken)
        {
            _privateToken = privateToken;
        }

        public IEnumerable<Group> GetGroups()
        {
            try
            {
                var response = new WebClient().DownloadString(ApiUrl + "/groups?per_page=1000&private_token=" + _privateToken);

                return JsonConvert.DeserializeObject<List<Group>>(response);
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<Project> GetProjects()
        {
            try
            {
                var response = new WebClient().DownloadString(ApiUrl + "/projects?per_page=1000&private_token=" + _privateToken);

                return JsonConvert.DeserializeObject<List<Project>>(response);
            }
            catch
            {
                return null;
            }
        }

        public bool CreateProject(string path, int namespaceId = -1)
        {
            try
            {
                string response = new WebClient().UploadString(ApiUrl +
                                                               "/projects" +
                                                               "?private_token=" + _privateToken +
                                                               "&path=" + path +
                                                               (namespaceId != -1 ? $"&namespace_id={namespaceId}" : ""),
                    "");

                return !response.Contains("message");
            }
            catch
            {
                return false;
            }
        }
    }

    public class Group
    {
        public int id { get; set; }
        public string path { get; set; }
    }


    public class User
    {
        public string name { get; set; }
        public string username { get; set; }
    }

    public class Project
    {
        public string default_branch { get; set; }
        public string ssh_url_to_repo { get; set; }
        public string http_url_to_repo { get; set; }
        public string name { get; set; }
        public string name_with_namespace { get; set; }
        public DateTime last_activity_at { get; set; }

        public GitRepository ToGitRepository(string localPath, bool useSsh = true)
        {
            return new GitRepository(this.name, localPath, useSsh ? this.ssh_url_to_repo : this.http_url_to_repo)
            {
                Branch = this.default_branch
            };
        }

    }
}