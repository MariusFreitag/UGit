﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace UGit
{
    class Helper
    {
        public static bool CheckInternetConnection()
        {
            try
            {
                new WebClient().DownloadString("http://google.com");
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool ShowYesNoDialog(string msg, string caption = "")
        {
            return MessageBox.Show(msg, caption, MessageBoxButton.YesNo) == MessageBoxResult.Yes;
        }
    }
}