﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace UGit
{
    public partial class RepositoryStripControl : UserControl
    {
        private GitRepository _repository;
        private RepositoryOverviewControl _parent;

        private bool _hasFocus = false;
        public bool HasFocus
        {
            get
            {
                return _hasFocus;
            }

            set
            {
                _hasFocus = value;
                this.Background = _hasFocus ? Brushes.LightBlue : Brushes.Transparent;
            }
        }

        public RepositoryStripControl(GitRepository repository, RepositoryOverviewControl parent)
        {
            InitializeComponent();

            this._repository = repository;
            this._parent = parent;

            Refresh();
        }

        public void Refresh()
        {
            LblTitle.Content = _repository.Name;

            UpdateStatus();
        }

        public GitRepository GetRepository()
        {
            return _repository;
        }

        private void UpdateStatus()
        {
            LblTitle.Content = "⌛ " + _repository.Name;
            LblTitle.Foreground = Brushes.DodgerBlue;

            Task.Run((Action)_repository.UpdateRepository)
                .ContinueWith(((task) => _repository.GetStatus()))
                .ContinueWith(
                    (statusTask) =>
                    {
                        var status = statusTask.Result;

                        var prefix = "";
                        var color = Brushes.Black;

                        if (status.IsCloned)
                        {
                            if (status.IsChanged)
                            {
                                prefix = "✎ ";
                                color = Brushes.Red;
                            }

                            if (status.IsDiverged || status.IsAhead)
                            {
                                prefix += "▲ ";
                                color = Brushes.Red;
                            }

                            if (status.IsDiverged || status.IsBehind)
                            {
                                prefix += "▼ ";
                                color = Brushes.Red;
                            }

                            if (!status.IsConnected)
                            {
                                prefix += "⚡ ";
                            }
                        }
                        else
                        {
                            color = Brushes.LightGray;
                        }

                        LblTitle.Content = prefix + _repository.Name;
                        LblTitle.Foreground = color;
                    },
                TaskScheduler.FromCurrentSynchronizationContext()
            );
        }

        private void Strip_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ProgressWindow.ShowWindow();

            while (!_repository.GetStatus().IsCloned)
            {
                if (Helper.ShowYesNoDialog("This repository is not cloned yet. Do you want to clone it?"))
                {
                    _repository.Clone();
                }
                else
                {
                    ProgressWindow.HideWindow();
                    return;
                }
            }

            _parent.Focus(_repository);
            Refresh();

            ProgressWindow.HideWindow();
        }

        private void BtnExplorer_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("explorer.exe", _repository.Path);
        }

        private void BtnGitConsole_Click(object sender, RoutedEventArgs e)
        {
            _repository.OpenGitConsole();
        }

        private void BtnSettings_Click(object sender, RoutedEventArgs e)
        {
            _repository = EditRepositoryWindow.EditRepository(_repository);

            if (_repository.Name == null)
            {
                _parent.Delete(_repository);
                _parent.Focus(null);
            }
            else
            {
                _parent.Refresh(_repository);
            }
        }
    }
}