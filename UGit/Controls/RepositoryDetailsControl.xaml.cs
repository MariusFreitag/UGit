﻿using System;
using System.Runtime.Remoting.Messaging;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace UGit
{
    public partial class RepositoryDetailsControl : UserControl
    {
        private GitRepository _repository;
        public RepositoryOverviewControl OverviewControl { get; set; }

        public RepositoryDetailsControl()
        {
            InitializeComponent();

            SetEnabled(false);
        }

        public void SetEnabled(bool isEnabled)
        {
            this.IsEnabled = isEnabled;
            this.Visibility = isEnabled ? Visibility.Visible : Visibility.Hidden;
        }

        public void Refresh()
        {
            if (IsEnabled)
            {
                ProgressWindow.ShowWindow();
                LoadRepository(_repository);
                ProgressWindow.HideWindow();
            }
        }

        public void LoadRepository(GitRepository repository)
        {
            this._repository = repository;

            #region Initialize detailsControl

            SetEnabled(true);
            Clear();

            #endregion Initialize detailsControl

            #region Print changes

            TxtChanges.Document.Blocks.Clear();
            TxtChanges.Visibility = Visibility.Visible;

            _repository.UpdateRepository();
            var status = _repository.GetStatus();

            if (!status.IsConnected)
            {
                AppendToRichTextBox(TxtChanges, Colors.Black, TxtChanges.FontSize, "Offline\r");
            }

            var filteredStatus = "";
            foreach (var line in Regex.Split(status.Raw, "\r\n|\r|\n"))
            {
                if (!string.IsNullOrWhiteSpace(line))
                {
                    // Only add a small line break
                    filteredStatus += (string.IsNullOrWhiteSpace(filteredStatus) ? "" : "\r") + line;
                }
            }
            AppendToRichTextBox(TxtChanges, Colors.Red, TxtChanges.FontSize, filteredStatus);

            TxtChanges.Visibility = status.IsChanged || status.IsDiverged || status.IsAhead || status.IsBehind || !status.IsConnected ? Visibility.Visible : Visibility.Collapsed;

            #endregion Print changes

            #region Print Commits

            TxtCommits.Document.Blocks.Clear();

            foreach (string line in Regex.Split(repository.GetLog(), "\r\n|\r|\n"))
            {
                var color = Colors.Black;
                var indent = true;

                if (line.StartsWith("commit"))
                {
                    color = Colors.SaddleBrown;
                    indent = false;
                }
                else if (line.StartsWith("Author") || line.StartsWith("Date"))
                {
                    color = Colors.Gray;
                    indent = false;
                }
                else if (line.StartsWith("  "))
                {
                    color = Colors.Green;
                    indent = false;
                }

                AppendToRichTextBox(TxtCommits, color, TxtChanges.FontSize, (indent ? "\t" : "") + line.Trim() + "\r");
            }

            #endregion Print Commits
        }

        private void Clear()
        {
            TxtCommits.Document.Blocks.Clear();
            TxtChanges.Document.Blocks.Clear();
        }

        private static void AppendToRichTextBox(RichTextBox richTextBox, Color color, double size, string text)
        {
            Brush brush = new SolidColorBrush(color);
            TextRange range = new TextRange(richTextBox.Document.ContentEnd, richTextBox.Document.ContentEnd);
            range.Text = text;
            range.ApplyPropertyValue(TextElement.ForegroundProperty, brush);
            range.ApplyPropertyValue(TextElement.FontSizeProperty, size);
        }
    }
}