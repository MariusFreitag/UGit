﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UGit
{
    public partial class RepositoryOverviewControl : UserControl
    {
        public List<GitRepository> Repositories;
        public RepositoryDetailsControl DetailsControl { get; set; }

        public RepositoryOverviewControl(List<GitRepository> repositories, RepositoryDetailsControl detailsControl)
        {
            InitializeComponent();

            this.Repositories = repositories;
            this.DetailsControl = detailsControl;

            RefreshAll();
        }

        public void Add(GitRepository repository)
        {
            Repositories.Add(repository);
            RefreshAll();
        }

        public void Delete(GitRepository repository)
        {
            Repositories.Remove(repository);
            foreach (var item in StackPanel.Children)
            {
                if (((RepositoryStripControl)item).GetRepository() == repository)
                {
                    // Remove corresponsing repository strip
                    StackPanel.Children.Remove((UIElement)item);

                    // Hide details control if shown repository is to be deleted
                    if(((RepositoryStripControl)item).HasFocus)
                    {
                        DetailsControl.SetEnabled(false);
                    }

                    return;
                }
            }
        }

        public void Refresh(GitRepository repository)
        {
            foreach (var item in StackPanel.Children)
            {
                if (((RepositoryStripControl)item).GetRepository() == repository)
                {
                    ((RepositoryStripControl)item).Refresh();

                    if (((RepositoryStripControl)item).HasFocus)
                    {
                        DetailsControl.SetEnabled(true);
                        DetailsControl.LoadRepository(repository);
                    }

                    break;
                }
            }
        }

        public void RefreshAll()
        {
            StackPanel.Children.Clear();
            DetailsControl.SetEnabled(false);

            Repositories = Repositories.OrderBy(o => o.Name).ToList();

            foreach (var repository in Repositories)
            {
                RepositoryStripControl newStrip = new RepositoryStripControl(repository, this);
                newStrip.Width = StackPanel.Width;
                StackPanel.Children.Add(newStrip);
            }
        }

        public void Focus(GitRepository repository)
        {
            foreach (var item in StackPanel.Children)
            {
                ((RepositoryStripControl)item).HasFocus = false;

                if (((RepositoryStripControl)item).GetRepository() == repository)
                {
                    ((RepositoryStripControl)item).HasFocus = true;
                    DetailsControl.SetEnabled(true);
                    DetailsControl.LoadRepository(repository);
                }
            }
        }
    }
}
