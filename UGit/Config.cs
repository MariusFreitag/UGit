﻿using System.Collections.Generic;

namespace UGit
{
    public class Config
    {
        public string GitLabApiKey { get; set; } = "";

        public bool ImportUseSsh { get; set; } = true;

        public Credentials Credentials { get; set; } = new Credentials("", "");

        public List<GitRepository> Repositories { get; set; } = new List<GitRepository>();
    }
}