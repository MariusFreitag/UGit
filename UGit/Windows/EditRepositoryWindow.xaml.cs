﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UGit
{
    internal partial class EditRepositoryWindow : Window
    {
        private GitRepository _repository;

        public static GitRepository EditRepository(GitRepository repository)
        {
            repository = repository ?? new GitRepository("", "", "");

            new EditRepositoryWindow(repository).ShowDialog();

            return repository;
        }

        public EditRepositoryWindow(GitRepository repository)
        {
            InitializeComponent();

            this._repository = repository;
            
            TxtName.Text = this._repository.Name;
            TxtPath.Text = this._repository.Path;
            TxtRemoteUri.Text = this._repository.RemoteUri;
            TxtRemoteName.Text = this._repository.RemoteName;
            TxtBranch.Text = this._repository.Branch;
        }

        private void LblDelete_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (Helper.ShowYesNoDialog("Do you really want to delete this repository from UGit?"))
            {
                _repository.Name = null;
                _repository.Path = null;
                _repository.RemoteUri = null;
                _repository.RemoteName = null;
                _repository.Branch = null;
                this.Close();
            }
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(TxtName.Text) || string.IsNullOrEmpty(TxtPath.Text))
            {
                MessageBox.Show("Name and path must be filled!");
                return;
            }

            _repository.Name = TxtName.Text;
            _repository.Path = TxtPath.Text;
            _repository.RemoteUri = TxtRemoteUri.Text;
            _repository.RemoteName = TxtRemoteName.Text;
            _repository.Branch = TxtBranch.Text;

            this.Close();
        }
    }
}