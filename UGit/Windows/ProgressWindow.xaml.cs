﻿using System;
using System.Management.Instrumentation;
using System.Runtime.Remoting.Contexts;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace UGit
{
    public partial class ProgressWindow : Window
    {
        private static ProgressWindow _instance;
        public static Window StaticOwner { get; set; }

        private ProgressWindow()
        {
            InitializeComponent();
        }

        public static void ShowWindow()
        {
            StaticOwner.IsEnabled = false;

            if (_instance == null)
            {
                _instance = new ProgressWindow();
                _instance.Owner = StaticOwner;
                _instance.Show();
            }
        }

        public static void HideWindow()
        {
            if (_instance != null)
            {
                _instance.Close();
                _instance = null;
            }

            StaticOwner.IsEnabled = true;
        }

        private new void Close()
        {
            // To prevent aborting the closing process
            this.Closing -= Window_Closing;
            base.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
        }
    }
}