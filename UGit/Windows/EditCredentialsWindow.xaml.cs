﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UGit
{
    internal partial class EditCredentialsWindow : Window
    {
        private Config _config;

        public EditCredentialsWindow(Config config)
        {
            InitializeComponent();

            this._config = config;
            TxtCredentialsName.Text = config.Credentials.Name;
            TxtCredentialsEmail.Text = config.Credentials.Email;
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(TxtCredentialsName.Text) || string.IsNullOrEmpty(TxtCredentialsEmail.Text))
            {
                MessageBox.Show("All fields must be filled!");
                return;
            }

            try
            {
                new System.Net.Mail.MailAddress(TxtCredentialsEmail.Text);
            }
            catch
            {

                MessageBox.Show("The email address is invalid!");
                return;
            }

            _config.Credentials.Name = TxtCredentialsName.Text;
            _config.Credentials.Email = TxtCredentialsEmail.Text;

            this.Close();
        }
    }
}
