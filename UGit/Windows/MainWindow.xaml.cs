﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;

namespace UGit
{
    public partial class MainWindow : Window
    {
        private Config _config;
        private RepositoryOverviewControl _repositoryOverviewControl;
        private RepositoryDetailsControl _repositoryDetailsControl;

        private readonly string _configPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location),
            "UGit.config");

        public MainWindow()
        {
            InitializeComponent();

            #region Check For Git
            if (!Environment.GetEnvironmentVariable("PATH").ToLower().Contains("git"))
            {
                MessageBox.Show("You need to install \"git\" in order to use UGit (download at \"git-scm.com\")", "UGit");
                Environment.Exit(0);
            }
            #endregion

            ProgressWindow.StaticOwner = this;

            ReadConfig();

            GitRepository.Credentials = _config.Credentials;

            _repositoryDetailsControl = new RepositoryDetailsControl();
            _repositoryOverviewControl = new RepositoryOverviewControl(_config.Repositories, _repositoryDetailsControl);

            _repositoryDetailsControl.OverviewControl = _repositoryOverviewControl;

            OverviewGrid.Content = _repositoryOverviewControl;
            RepositoryDetailsGrid.Children.Add(_repositoryDetailsControl);
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            GitRepository newRepository = EditRepositoryWindow.EditRepository(null);

            if (!string.IsNullOrEmpty(newRepository.Name))
            {
                _repositoryOverviewControl.Add(newRepository);
                WriteConfig();
            }
        }

        private void BtnGitLab_Click(object sender, RoutedEventArgs e)
        {
            new Windows.GitLabApiWindow(_repositoryOverviewControl, _config).ShowDialog();
            BtnRefresh_Click(null, null);
        }

        private void BtnRefresh_Click(object sender, RoutedEventArgs e)
        {
            WriteConfig();
            _repositoryOverviewControl.RefreshAll();
        }

        private void LblGit_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Process.Start("http://git-scm.com");
        }

        private void BtnInfo_Click(object sender, RoutedEventArgs e)
        {
            new InfoWindow().ShowDialog();
        }

        private void LblCreatedBy_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Process.Start("https://mariusfreitag.de");
        }


        private void BtnCredentials_Click(object sender, RoutedEventArgs e)
        {
            new EditCredentialsWindow(_config).ShowDialog();
            WriteConfig();
        }

        private void ReadConfig()
        {
            #region Read Config File

            try
            {
                _config = JsonConvert.DeserializeObject<Config>(File.ReadAllText(_configPath));
            }
            catch (Exception)
            {
            }
            _config = _config ?? new Config();

            #endregion

            while (string.IsNullOrEmpty(_config.Credentials.Email) || string.IsNullOrEmpty(_config.Credentials.Name))
            {
                MessageBox.Show("Your credentials are not configured!", "UGit");
                new EditCredentialsWindow(_config).ShowDialog();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            WriteConfig();
        }

        private void WriteConfig()
        {
            _config.Repositories = _repositoryOverviewControl.Repositories;

            if (!Directory.Exists(Path.GetDirectoryName(_configPath)))
                Directory.CreateDirectory(Path.GetDirectoryName(_configPath));

            if (!File.Exists(_configPath) || File.ReadAllText(_configPath) != JsonConvert.SerializeObject(_config, Formatting.Indented))
                File.WriteAllText(_configPath, JsonConvert.SerializeObject(_config, Formatting.Indented));
        }
    }
}