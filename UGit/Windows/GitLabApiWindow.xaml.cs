﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace UGit.Windows
{
    public partial class GitLabApiWindow : Window
    {
        private RepositoryOverviewControl _repositoryOverviewControl;
        private Config _config;
        private Dictionary<int, Group> _groups;
        private Dictionary<int, Project> _projects;
        private GitLab _gitLab;

        public GitLabApiWindow(RepositoryOverviewControl repositoryOverviewControl, Config config)
        {
            InitializeComponent();

            this._repositoryOverviewControl = repositoryOverviewControl;
            this._config = config;

            TxtApiKey.Password = _config.GitLabApiKey;

            if (_config.ImportUseSsh)
            {
                RbSsh.IsChecked = true;
            }
            else
            {
                RbHttp.IsChecked = true;
            }
        }

        private void Save()
        {
            _config.GitLabApiKey = TxtApiKey.Password;
            _config.ImportUseSsh = RbSsh.IsChecked == true;
        }

        private void ReloadGroups()
        {
            if (!GrpCreate.IsEnabled)
            {
                return;
            }

            _groups = new Dictionary<int, Group>();
            CbxGroup.Items.Clear();

            var dash = new ComboBoxItem();
            dash.Content = "-";
            _groups.Add(dash.GetHashCode(), null);

            CbxGroup.Items.Add(dash);
            CbxGroup.SelectedIndex = 0;

            GrpCreate.IsEnabled = false;

            Task.Run(() => _gitLab.GetGroups())
                .ContinueWith(
                    (groupsTask) =>
                    {
                        if (groupsTask.Result == null)
                        {
                            return;
                        }

                        foreach (var group in groupsTask.Result)
                        {
                            var item = new ComboBoxItem();
                            item.Content = group.path;
                            CbxGroup.Items.Add(item);
                            _groups.Add(item.GetHashCode(), group);
                        }
                        GrpCreate.IsEnabled = true;
                    },
                    TaskScheduler.FromCurrentSynchronizationContext()
           );
        }

        private void ReloadProjects()
        {
            if (!GrpImport.IsEnabled)
            {
                return;
            }

            GrpImport.IsEnabled = false;
            PbImport.Visibility = Visibility.Visible;

            ProjectsGrid.Children.Clear();
            this._projects = new Dictionary<int, Project>();

            Task.Run(() => _gitLab.GetProjects())
            .ContinueWith(
                (projectsTask) =>
                {
                    if (projectsTask.Result == null)
                    {
                        return;
                    }

                    List<Project> projects = projectsTask.Result.ToList();
                    projects.Sort((a, b) => b.last_activity_at.CompareTo(a.last_activity_at));

                    foreach (var project in projects)
                    {
                        var checkBox = new CheckBox();
                        checkBox.Content =
                            $"{project.name_with_namespace} (last changed {project.last_activity_at.ToShortDateString()})";
                        checkBox.IsEnabled = _repositoryOverviewControl.Repositories.All(x => x.Name != project.name);
                        ProjectsGrid.Children.Add(checkBox);
                        _projects.Add(checkBox.GetHashCode(), project);
                    }

                    GrpImport.IsEnabled = true;
                    PbImport.Visibility = Visibility.Collapsed;
                },
                TaskScheduler.FromCurrentSynchronizationContext()
        );
        }

        private void BtnCreate_Click(object sender, RoutedEventArgs e)
        {
            if (_gitLab == null)
            {
                MessageBox.Show("Please check your API-Key and try again!");
                return;
            }

            GrpCreate.IsEnabled = false;

            var group = this._groups[((ComboBoxItem)CbxGroup.SelectedItem).GetHashCode()];
            string name = TxtName.Text;

            try
            {
                Task.Run(() => _gitLab.CreateProject(name, group?.id ?? -1))
                    .ContinueWith(
                        (createProjectTask) =>
                        {
                            if (createProjectTask.Result)
                            {
                                MessageBox.Show("Project created successfully");
                            }
                            else
                            {
                                MessageBox.Show("Project creation failed");
                            }

                            GrpCreate.IsEnabled = true;
                            ReloadProjects();
                        },
                        TaskScheduler.FromCurrentSynchronizationContext()
                   );
            }
            catch
            {

            }
        }

        private void BtnImport_Click(object sender, RoutedEventArgs e)
        {
            if (_gitLab == null)
            {
                MessageBox.Show("Please check your API-Key and try again!");
                return;
            }

            GrpImport.IsEnabled = false;

            foreach (CheckBox checkBox in ProjectsGrid.Children)
            {
                if (checkBox.IsChecked == true)
                {
                    var project = this._projects[checkBox.GetHashCode()];
                    var gitRepository =
                        project.ToGitRepository(
                            System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                                "Projects", project.name), RbSsh.IsChecked == true);
                    gitRepository = EditRepositoryWindow.EditRepository(gitRepository);

                    _repositoryOverviewControl.Repositories.Add(gitRepository);
                }
            }

            GrpImport.IsEnabled = true;

            this.Close();
        }

        private void TxtApiKey_PasswordChanged(object sender, RoutedEventArgs e)
        {
            DispatcherTimer dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += (object dSender, EventArgs dE) =>
            {
                ((DispatcherTimer)dSender).Stop();
                try
                {
                    _gitLab = new GitLab(TxtApiKey.Password);
                    ReloadGroups();
                    ReloadProjects();
                    Save();
                }
                catch
                {
                    _gitLab = null;
                }
            };
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Save();
        }
    }
}